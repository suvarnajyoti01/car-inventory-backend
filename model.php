<?php
	class Model {
		
		public function __construct() {
			$this->database = new Database();
			$this->tableName = 'model';
			$this->parentTable = 'manufacturer';
    }
    
    public function modelOperations($request, $files) {
			$type = isset($request['type']) ? $request['type'] : NULL;

			if ( $type == 'createModel' ) {
				$this->createModel($request, $files);

			} elseif ( $type == 'updateModel' ) {
				$this->updateModel($request, $files);

			} elseif ( $type == 'getModels' ) {
				$this->getModels($request);

			} elseif ( $type == 'getAllModelDetails' ) {
				$this->getAllModelDetails();

			} else {
				$this->deleteModel($request);
			}
		}

    public function getModels($request) {
			$id = isset($request['id']) ? $request['id'] : NULL;
			$result = $this->database->read($this->tableName, $id);
      $rows = array();
      
			while($r = mysqli_fetch_assoc($result['data'])) {
				$rows[] = $r;
			}
			print json_encode($rows);
		}
		
		public function getAllModelDetails() {
			$result = $this->database->read($this->tableName, null, $this->parentTable);
      $rows = array();
      
			while($r = mysqli_fetch_assoc($result['data'])) {
				$rows[] = $r;
			}
			print json_encode($rows);
    }
    
    public function createModel($request, $files) {
			$data = array();
			foreach ($request as $key => $value) {
				if ($key !== 'op' && $key !== 'type' ) {
					$data[$key] = $value;
				}
      }
      $images = count($files) > 0 ? $files : NULL;
				
			try {
				$result = $this->database->create($this->tableName, $data, $images);
				print json_encode($result);
			} catch (ValidationException $e) {
				print json_encode($e->getErrors());
			}
		}

		public function updateModel($request, $files) {
			$data = array();
			foreach ($request as $key => $value) {
				if ($key !== 'op' && $key !== 'type' ) {
					$data[$key] = $value;
				}
			}
      $images = count($files) > 0 ? $files : NULL;
				
			try {
				$result = $this->database->update($this->tableName, $data, $request['id'], $images);
				print json_encode($result);
			} catch (ValidationException $e) {
				print json_encode($e->getErrors());
			}
		}

		public function deleteModel($request) {
      $id = isset($request['id']) ? $request['id'] : NULL;
			if ($id) {
				$result = $this->database->delete($this->tableName, $id);
				print json_encode($result);
			}
		}
	}
?>
