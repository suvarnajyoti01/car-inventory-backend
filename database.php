<?php

	class Database {
		
		private $connection;

		function __construct() {
			$this->connect_db();
		}

		public function connect_db(){
			$this->connection = mysqli_connect('localhost', 'root', '', 'car_inventory');
			if (mysqli_connect_error()) {
				die("Database Connection Failed" . mysqli_connect_error() . mysqli_connect_errno());
			}
		}

		public function create($table, $data, $files = NULL) {
			$data = json_decode($data['data'], true);
			$result = [];
			$key = array_keys($data);
			$val = array_values($data);
			$sql = "INSERT INTO $table (" . implode(', ', $key) . ") "
			. "VALUES ('" . implode("', '", $val) . "')";
			$res = mysqli_query($this->connection, $sql);
			if ($res) {
				if ($table == 'model') {
					$last_id = mysqli_insert_id($this->connection);
					if($files && count($files) > 0) {
						$result2 =	$this->saveImage($table, $last_id, $files); 

						if ($result2['status']) {
							$result['status'] = true;
						} else {
							$result['status'] = false;
						}
					} else {
						$result['status'] = true;
					}
				} else {
					$result['status'] = true;
				}
			} else {
				$result['err'] = $this->connection->error;
				$result['status'] = false;
			}
			return  $result;
		}

		public function read($table, $id = null, $p_table = null) {
			$result = [];
			$sql = "SELECT * FROM $table";
			if ($id) { 
				$sql .= " WHERE id=$id";
			} else if ($p_table) {
				$sql = "SELECT $p_table.name as m_name, $table.*, COUNT(*) as count
				FROM $p_table
				INNER JOIN $table ON $p_table.id=$table.manufacturer_id GROUP BY $table.color, $table.manufacturing_year, $table.name";
			}
			$res = mysqli_query($this->connection, $sql);
			if ($res) {
				$result['data'] = $res;
				$result['status'] = true;
			} else {
				$result['err'] = $this->connection->error;
				$result['status'] = false;
			}
			return  $result;
		}

		public function update($table, $data, $id, $files = NULL) {
			$data = json_decode($data['data'], true);
			$result = [];
			$query = "UPDATE ". $table ." SET";
			$comma = " ";
			foreach ($data as $key => $val) {
				if (!empty($val)) {
					$query .= $comma . $key . " = '" . mysqli_real_escape_string($this->connection, trim($val)) . "'";
					$comma = ", ";
				}
			}
			$query .= 'WHERE id = '. $id;
			$res = mysqli_query($this->connection, $query);
			if ($res) {
				if ($table == 'model') {
					if($files && count($files) > 0) {
						$result2 =	$this->saveImage($table, $id, $files); 
						if ($result2['status']) {
							$result['status'] = true;
						} else {
							$result['status'] = false;
						}
					} else {
						$result['status'] = true;
					}
				} else {
					$result['status'] = true;
				}
			} else {
				$result['err'] = $this->connection->error;
				$result['status'] = false;
			}
			return  $result;
		}

		public function delete($table, $id) {
			$result = [];
			$sql = "DELETE FROM $table WHERE id=$id";
			$res = mysqli_query($this->connection, $sql);
			if ($res) {
				if ($table == 'model') {
					$this->deleteFolder($id);
				}
				$result['status'] = true;
			} else {
				$result['err'] = $this->connection->error;
				$result['status'] = false;
			}
			return  $result;
		}

		public function saveImage($table, $id, $data) {
			if (count($data) > 0) {
				foreach ($data as $key => $val) {
					if ($data[$key]["error"] != 0) {
						$result['status'] = false;
						$result['err'] = "";
					} else {
						if (!file_exists('images')) {
							mkdir('images', 0777, true);
						}
						if (!file_exists('images/'.$id)) {
							mkdir('images/'.$id);
						}
						
						if (move_uploaded_file($data[$key]["tmp_name"],"images/" .$id."/". $data[$key]["name"])) {
							$file="images/".$id."/".$data[$key]["name"];
							$sql="UPDATE ". $table. " SET ". $key ." = '".$file."' WHERE id=".$id ;
							$res = mysqli_query($this->connection, $sql);
							if ($res) {
								$result['status'] = true;
							} else {
								$result['err'] = $this->connection->error;
								$result['status'] = false;
							}
						}
					}
				}
				return $result;
			}
		}

		public function deleteFolder($id) {
			$dir = 'images/'.$id;
			$it = new RecursiveDirectoryIterator($dir, RecursiveDirectoryIterator::SKIP_DOTS);
			$files = new RecursiveIteratorIterator($it, RecursiveIteratorIterator::CHILD_FIRST);
			foreach($files as $file) {
				if ($file->isDir()){
					rmdir($file->getRealPath());
				} else {
					unlink($file->getRealPath());
				}
			}
			rmdir($dir);
		}
	}
?>