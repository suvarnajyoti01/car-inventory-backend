<?php
  require_once 'database.php';
  require_once 'manufacturer.php';
  require_once 'model.php';

  class RoutesController {

    public function __construct() {
      $this->manufacturer = new Manufacturer();
      $this->model = new Model();
    }

    public function handleRequest() {
      $op = isset($_REQUEST['op']) ? $_REQUEST['op'] : NULL;

      try {
        if ( !$op || $op == 'manufacturer' ) {
          $this->manufacturer->manufacturerOperations($_REQUEST);
        } elseif ( $op == 'model' ) {
          $this->model->modelOperations($_REQUEST, $_FILES);
        } else {
          $this->showError("Page not found", "Page for operation ".$op." was not found!");
        }
      } catch ( Exception $e ) {
        // some unknown Exception got through here, use application error page to display it
        print_r("Application error", $e->getMessage());
      }
    }
  }
?>
