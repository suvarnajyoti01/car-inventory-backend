<?php
	class Manufacturer {
		
		public function __construct() {
			$this->database = new Database();
			$this->tableName = 'manufacturer';
    }
    
    public function manufacturerOperations($request) {
			$type = isset($request['type']) ? $request['type'] : NULL;

			if ( $type == 'addManufacturer' ) {
				$this->addManufacturer($request);

			} elseif ( $type == 'updateManufacturer' ) {
				$this->updateManufacturer($request);

			} elseif ( $type == 'getManufacturers' ) {
				$this->getManufacturers($request);

			} else {
				$this->deleteManufacturer($request);
			}
		}

    public function getManufacturers($request) {
			$id = isset($request['id']) ? $request['id'] : NULL;
			$result = $this->database->read($this->tableName, $id);
			$rows = array();
			while($r = mysqli_fetch_assoc($result['data'])) {
				$rows[] = $r;
			}
			print json_encode($rows);
    }
    
    public function addManufacturer($request) {
			$data = array();
			foreach ($request as $key => $value) {
				if ($key !== 'op' && $key !== 'type' ) {
					$data[$key] = $value;
				}
			}
				
			try {
				$result = $this->database->create($this->tableName, $data);
				print json_encode($result);
			} catch (ValidationException $e) {
				print json_encode($e->getErrors());
			}
		}

		public function updateManufacturer($request) {
			$data = array();
			foreach ($request as $key => $value) {
				if ($key !== 'op' && $key !== 'type' ) {
					$data[$key] = $value;
				}
			}
				
			try {
				$result = $this->database->update($this->tableName, $data, $request['id']);
				print json_encode($result);
			} catch (ValidationException $e) {
				print json_encode($e->getErrors());
			}
		}

		public function deleteManufacturer($request) {
			$id = isset($request['id']) ? $request['id'] : NULL;
			if ($id) {
				$result = $this->database->delete($this->tableName, $id);
				print json_encode($result);
			}
		}
	}
?>
