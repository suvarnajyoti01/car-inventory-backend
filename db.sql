    
CREATE TABLE `car_inventory`.`manufacturer` ( `id` INT NOT NULL AUTO_INCREMENT , `name` VARCHAR(100) NOT NULL , PRIMARY KEY (`id`), created_on TIMESTAMP NOT NULL ) ENGINE = InnoDB;

CREATE TABLE model (
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(100) NOT NULL,
    color VARCHAR(100) NOT NULL,
    manufacturing_year SMALLINT NOT NULL, 
    registration_number INT NOT NULL, 
    note VARCHAR(300) NOT NULL, 
    document_1 VARCHAR(150) NOT NULL,
    document_2 VARCHAR(150) NOT NULL,
    manufacturer_id int NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (manufacturer_id) REFERENCES manufacturer(id),
    created_on TIMESTAMP NOT NULL
);